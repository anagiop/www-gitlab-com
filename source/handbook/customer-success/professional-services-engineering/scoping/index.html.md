---
layout: markdown_page
title: "Scoping Professional Services"
---
# Selling Professional Services
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

## Getting Started
Services engagements can take [three forms](/handbook/customer-success/professional-services-engineering/#selling-professional-services). These include SKUs, standard SOWs, and custom SOWs. For custom SOWs, the [workflow for SOW creation]() involves a partnership between the SA/TAM and the Professional Services Engineering team.

This page is designed to collect details around the types of questions that are valuable in discovering the customer's required capabilities. That will help the team align the SOW with the positive business outcomes the customer is looking for with a services engagement.  While not an exhaustive list, these questions, and suggestions will help spark the discovery conversations.

## Migrations
Migrations are one of the most complex types of services in any technical field.  Systems store data in a variety of ways that evolves.  Also, customers and users often use the same data model and system to represent completely different logical units to their teams.  To ensure a transition that meets the customer's needs, we want to make sure we understand their usage of their current systems.

### SVN to Git Scoping questions
SVN to Git Questions

1. What is the structure of the SVN repos and subprojects? Do they follow the "standard" of:
    ```
        Repository
            Project 1
                branches/trunks/tags
            Project 2
                branches/trunks/tags
    ```
    1. Any other variations? Example:
        ```
            Trunk
                Project 1
                Project 2
            Tags
                tag name
                    Project 1
                    Project 2
            Branches
                Branch name
                    Project 1
                    Project 2
        ```
1. How many SVN repositories are there? How are they broken up?
1. What is the overall size of the SVN repos?
1. How much history (e.g. tags, branches, etc.) should be migrated?
1. Are any binary files stored in SVN?
1. How are you currently using SVN externals? Do you have some example use cases?
